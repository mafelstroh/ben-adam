# ben-adam Web Application
## Author: Manuel Felipe Stroh - [LinkedIn](https://www.linkedin.com/in/mafelstroh/)

# Task
Description
> Help​ ​HR​ ​manager​ ​Personia​ ​get​ ​a​ ​grasp​ ​of​ ​her​ ​ever​ ​changing​ ​company’s​ ​hierarchy!​ ​Every​ ​week​ ​Personia 
receives​ ​a​ ​JSON​ ​of​ ​employees​ ​and​ ​their​ ​supervisors​ ​from​ ​her​ ​demanding​ ​CEO​ ​Chris,​ ​who​ ​keeps​ ​changing 
his​ ​mind​ ​about​ ​how​ ​to​ ​structure​ ​his​ ​company.​ ​Chris​ ​wants​ ​this​ ​JSON​ ​to​ ​be​ ​restructured,​ ​so​ ​that​ ​he​ ​can 
better​ ​see​ ​the​ ​employees​ ​hierarchy.​ ​Chris​ ​is​ ​especially​ ​happy​ ​if​ ​Personia​ ​provides​ ​him​ ​with​ ​a​ ​visual 
graphical​ ​representation​ ​of​ ​the​ ​hierarchy. 

Elements provided in this project:

 1. Laravel API implemented
 2. Sorting algorithm to help organize employee structure
 3. Web application with file upload feature
 4. Basic Unit Tests
 5. Detailed git workflow

**Setup and usage**

For this example, I've used Laravel Homestead as a development environment. A LAMP/LEMP setup is recommended.

1. Clone the project repository
	```
	git clone https://gitlab.com/mafelstroh/ben-adam.git
	```
2. Create a `.env` file with the following information (for both the database and application key)
	```
	APP_NAME=Laravel
	APP_ENV=local
	APP_KEY=base64:Qj9GE2JJHEw8Glir/9YmimgcrvAtS8B6G/BdcnYSGFY=
	APP_DEBUG=true
	APP_LOG_LEVEL=debug
	APP_URL=http://localhost

	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=homestead
	DB_USERNAME=homestead
	DB_PASSWORD=secret
	```
3. Alternatively, set a application key using the `artisan`command
	```
	php artisan key:generate
	```
4. Install dependencies using `composer` executing `composer install` inside the project root
5. Install front-end dependencies using `yarn && yarn dev` or `npm install && npm run dev`. This will also build the assets required for the web application.

**Usage:**

6. The API functionality can be easily tested using `curl` or a client such as PostMan via the exposed end-point:
```
	POST --> http://localhost/api/employee
```
8.  Via the web application, you can upload a text file with a JSON structure such as the shown below:
```
{
	"Pete": "Nick",
	"Barbara": "Nick",
	"Nick": "Sophie",
	"Sophie": "Jonas"
}
```

**Screenshots**

![Web Application](https://lh3.googleusercontent.com/zwVQQa6HGFXtRMXsEmyQDDqi_BESBubEe8QQ7-G8yexfWQTqF1shxzTdhN_lPy2LLBQBpe4o6qX_aQ=w1873-h965-rw)

![enter image description here](https://lh3.googleusercontent.com/PIQXzj_uMKGUuZhzsGVd8rg46LqZUnAloroI1xpr7hHOGerhYV6jadfyNBotGXi9Anb6ckPqFzVpJg=w1873-h965-rw)

![enter image description here](https://lh5.googleusercontent.com/RQr1eWqqOlq_JvRhSZYOmjmmwJlxPUVJ780u14h5fcto4Z0iQ6iU91TD6uljXKuBV5mQQDW8PYaP_g=w1873-h965)
