<?php

use App\Http\Resources\EmployeeResource;

Route::get('/', function () {
    return view('app');
});

// We will be using only this particular resource
Route::post('employee', 'EmployeeController@processUploadedFile');
