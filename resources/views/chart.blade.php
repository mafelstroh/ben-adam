<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >

        <title>Personio: ben-adam Web Application</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>

        <div class="container">
            <div class="alert alert-success" role="alert">Success! Your organization @ Personio looks like the one below!</div>
            <div id="chart-container"></div>
        </div>        

        <script src="{{ asset('js/app.js') }}"></script>
        <script>
            $(function() {
                let json_data = '{!! json_encode($hierarchy) !!}';
                let data_source = JSON.parse(json_data.substring(1, json_data.length-1));

                $('#chart-container').orgchart({
                    'data': data_source,
                    'nodeContent': 'title'
                });
            });                
        </script>
    </body>
</html>
