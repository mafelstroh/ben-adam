<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >

        <title>Personio: ben-adam Web Application</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app" class="container">
            <div class="panel panel-primary">
              <div class="panel-heading">
                <h2>Personio: ben-adam Web Application</h2>
            </div>
            
              <div class="panel-body">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{ $message }}</strong>
                    </div>
                @endif

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(array('url' => '/employee','files' => true)) !!}
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::file('data', array('class' => 'form-control')) !!}
                        </div>

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success">Upload and process!</button>
                        </div>
                    </div>
                {!! Form::close() !!}

              </div>
            </div>            
        </div>
    </body>
</html>
