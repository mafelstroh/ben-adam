<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    const DATA_FILE_NAME = 'raw_data.txt';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'supervisor',
    ];

    /**
     * toArray - Overrides default toArray() method in order to return
     *           only the information that we want to share for this resource.
     *			 This can be accomplished as well with a Resource class.
     *
     */
    public function toArray() {
    	return [
    		'name' 		 => $this->name,
    		'supervisor' => $this->supervisor
    	];
    }
}
