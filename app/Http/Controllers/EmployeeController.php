<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * getCompanyHierarcy
     *
     * @return \Illuminate\Http\Response
     */
    public function getCompanyHierarcy(Request $request)
    {
        $data = [];
        // Assuming that we'll have the proper data as required
        if ($request->has('data')) {
            $data = $this->_create($request->get('data'));
        }
        $hierarchy = $this->_createAndSortNestedStruct($data);
        
        return response()->json($hierarchy, 200);   
    }

    /**
     * processUploadedFile
     *
     * @return \Illuminate\Http\Response
     */
    public function processUploadedFile(Request $request)
    {
        // Basic, very basic validation
        request()->validate([
            'data' => 'required|file|mimes:txt'
        ]);

        // Use Laravel storage to ease the process
        $file_path = $request->file('data')->storeAs(
                'employees', Employee::DATA_FILE_NAME
            );
        $request->request->add(['data' => Storage::get($file_path)]);

        // Send the request instance to the method that
        // already takes care of the employee structure
        $hierarchy = $this->getCompanyHierarcy($request);

        return view('chart', ['hierarchy' => $hierarchy->getData()]);
    }    

    /**
     * _create
     *
     * @return Array
     */
    private function _create($baseData) {
    	$insert = [];
    	$rawEmployees = json_decode($baseData);

    	// Simply wipe the table in order to work
    	// with a clean "staging area"
		Employee::truncate();

    	// Create supervisors first
    	$rawSupervisors = array_unique((array)$rawEmployees);

    	foreach ($rawSupervisors as $v) {
    		$insert[] = [
    			'name'       => $v,
    			'supervisor' => null
    		];
    	}
    	Employee::insert($insert);

    	// Insert all employees, update supervisor
    	foreach ($rawEmployees as $name => $supervisor) {
    		$employee = Employee::firstOrNew(['name' => $name]);

    		$employee->supervisor = $supervisor;
    		$employee->save();
    	}

    	return Employee::all()->toArray();
    }

    /**
     * _createAndSortNestedStruct
     *
     * @return Array
     */
	private function _createAndSortNestedStruct(array $elements, $parentSup = null) {
	    $struct = [];

	    foreach ($elements as $element) {
	        if ($element['supervisor'] == $parentSup) {
	            $children = $this->_createAndSortNestedStruct($elements, $element['name']);

                $element['title']    = "Personio Rockstar"; // How about a friendly title? ;)
                $element['children'] = ($children) ? $children : [];

	            // This is done just to avoid confusing names
	            unset($element['supervisor']);
	            $struct[] = $element;
	        }
	    }
	    return $struct;
	}
}
