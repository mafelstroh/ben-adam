<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EmployeeControllerTest extends TestCase
{
    /**
     * A simple Unit Tets to include the benefits of
     * working with PHP Unit in Laravel.
     */
    public function testGetCompanyHierarchy()
    {
    	$testData = [
			"Pete"    => "Nick",
			"Barbara" => "Nick",
			"Nick"    => "Sophie",
			"Sophie"  => "Jonas"
		];

        $this->json('POST', 'api/employee', $testData)
        	->assertStatus(200);
    }
}
